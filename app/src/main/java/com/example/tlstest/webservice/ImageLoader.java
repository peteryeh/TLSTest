package com.example.tlstest.webservice;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import com.example.tlstest.util.Constants;

/**
 * Created by leo on 9/2/16.
 */
public class ImageLoader {

    private static final String TAG ="ImageLoader";
    private static Context mContext;
    private String imageUrl;
    private Bitmap mBitmap;

    public static ImageLoader getInstance(Context context){
        mContext = context;
        return  new ImageLoader();
    }




    public Bitmap loadImageFromUrl(String imageurl) {
        imageUrl = imageurl;
        AsyncTask task = new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                Bitmap bitmap = null;

                if (!NetTool.checkNet(mContext)) {
                    if(Constants.logEnable) Log.v(TAG, "net error");
                    return null;
                }

                try {
                    URL url = new URL(imageUrl);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.setConnectTimeout(10 * 1000);
                    connection.setReadTimeout(10 * 1000);
                    connection.connect();
                    if (connection.getResponseCode() == 200) {
                        InputStream is = connection.getInputStream();
                        bitmap = BitmapFactory.decodeStream(is);
                    }
                } catch (Exception e) {
                   if(Constants.logEnable) Log.e(TAG,"Exception:"+e.getMessage());
                }

                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                mBitmap = bitmap;
            }
        }.execute();

        try {
            return (Bitmap)task.get();
        }catch(InterruptedException |ExecutionException e){
            if(Constants.logEnable) Log.e(TAG,"exception:"+e.getMessage());
        }
        return null;
    }


}
