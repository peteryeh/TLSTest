package com.example.tlstest.webservice;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.example.tlstest.util.AndroidUtil;
import com.example.tlstest.util.Constants;
import com.example.tlstest.util.StringUtils;
import com.example.tlstest.util.TLSSocketFactory;

import javax.net.ssl.HttpsURLConnection;


public class ServiceCommunication extends AsyncTask<String, Integer, Object> {
	private static final String TAG = "ServiceCommunication";
	private static String cookieString = null;
	private Context mcontext;
	private ServiceParser parser;
	private String contentType;
	private static final String CONTENT_TYPE_TEXTJSON_WITH_UTF8 = "application/json;charset=utf-8";
	private int what;
	private int handleRequestName;
	private int responseCode = -1;
	private HttpURLConnection con;
	private URL url;

	private String methodType="POST";
	private boolean doOutput=true;
	private boolean isAssignURL = false;

	public ServiceCommunication(Context context, ServiceParser parser) {
		//if(Constants.logEnable)Log.i(TAG,"new ServiceCommunication");
		this.parser = parser;
		this.mcontext = context;

	}

	public ServiceCommunication(Context context, ServiceParser parser, boolean isAssignUrl) {
		//if(Constants.logEnable)Log.i(TAG,"new ServiceCommunication");

		this.parser = parser;
		this.mcontext = context;
		isAssignURL = isAssignUrl;
	}
	public ServiceCommunication(Context context, ServiceParser parser, boolean isAssignUrl, String methodType) {
		//if(Constants.logEnable)Log.i(TAG,"new ServiceCommunication");

		this.parser = parser;
		this.mcontext = context;
		isAssignURL = isAssignUrl;
		checkMethodType(methodType);
	}
	public ServiceCommunication(Context context, ServiceParser parser, String methodType) {
		//if(Constants.logEnable)Log.i(TAG,"new ServiceCommunication");

		this.parser = parser;
		this.mcontext = context;
		checkMethodType(methodType);
	}

	protected void onPreExecute() {
		//if(Constants.logEnable)Log.i(TAG,"onPreExecute");
	}

	@Override
	protected Object doInBackground(String... params) {
		if(con != null){
			con.disconnect();
			con = null;
		}
		OutputStreamWriter osw = null;
		InputStream receiveStream = null;
		ByteArrayOutputStream buffStream = null;


		try {
			what = Integer.parseInt(params[0]);
			handleRequestName= Integer.parseInt(params[0]);
			responseCode = -1;
			//TODO
			//parser.createTestDialog(1);
			if (!NetTool.checkNet(mcontext)) {
				if(Constants.logEnable) Log.v(TAG, "net error");
				what = Constants.NETWORK_CONNECTION_ERROR;
				return null;
			}
			//parser.createTestDialog(2);
			System.setProperty("http.keepAlive", "false");
			if (isAssignURL) {
				this.url = new URL(params[1]);
				if(Constants.logEnable) Log.d(TAG, "assign url:" + params[1]);
			} else {
				this.url = new URL(Constants.DEFAULT_IP + params[1]);
				if(Constants.logEnable) Log.d(TAG, "not assign url:" + params[1]);
			}
            if("https".equals(url.getProtocol())){
				if(Constants.logEnable) Log.d(TAG, "ssl https");
				con = (HttpsURLConnection) url.openConnection();
				((HttpsURLConnection)con).setSSLSocketFactory(new TLSSocketFactory());

//				if(Constants.logEnable) Log.d(TAG, "https but use normal mode");
//				con = (HttpURLConnection) url.openConnection();
            }
            else{
				if(Constants.logEnable) Log.d(TAG, "normal http");
				con = (HttpURLConnection) url.openConnection();
            }

			if(Constants.logEnable) Log.i(TAG, "URL: " + url);

			con.setRequestProperty("Content-Type", CONTENT_TYPE_TEXTJSON_WITH_UTF8);
			con.setRequestProperty("Accept", CONTENT_TYPE_TEXTJSON_WITH_UTF8);
			con.setRequestMethod(methodType);
			con.setDoOutput(doOutput);
			con.setDoInput(true);
			con.setUseCaches(false);
			con.setConnectTimeout(10 * 1000);
			if (doOutput && StringUtils.checkNULL(params[2])!=null) {
				osw = new OutputStreamWriter(con.getOutputStream(), "utf-8");
				osw.write(params[2]);
				osw.flush();
			}
			responseCode = con.getResponseCode();
			if(Constants.logEnable) Log.i(TAG, "responseCode :"+responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) {
				receiveStream = con.getInputStream();
				if (receiveStream != null) {
					contentType = con.getContentType();
					if(Constants.logEnable) Log.v(TAG, "contentType=" + contentType);
					if (CONTENT_TYPE_TEXTJSON_WITH_UTF8.equalsIgnoreCase(contentType)) {
						return AndroidUtil.covertInputStreamToString(receiveStream);
					}else{
						if(Constants.logEnable) Log.e(TAG, "content type error, should not be seen.");
						return  AndroidUtil.covertInputStreamToString(receiveStream);
					}
				}
			}
		} catch (Exception e) {
			if(Constants.logEnable) Log.e(TAG,"exception:"+e.getMessage());
		} finally {
			try {
				if (osw != null) {
					osw.close();
				}
				if (receiveStream != null) {
					receiveStream.close();
				}
				if (buffStream != null) {
					buffStream.close();
				}
				if (con != null) {
					con.disconnect();
					con = null;
				}
			} catch (Exception e) {
				if(Constants.logEnable) Log.e(TAG,"exception:"+e.getMessage());
			}
		}
		return null;
	}

	protected void onProgressUpdate(Integer... valule) {}

	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
		//parser.createTestDialog(6);
		//if(Constants.logEnable)Log.i(TAG, "onPostExecute");
		if(Constants.logEnable) Log.i(TAG,"result :"+(String)result);
		SeviceResponseBean resultBean = new SeviceResponseBean();
		resultBean.responseCode = responseCode;
		resultBean.what=what;
		resultBean.handleRequestName=handleRequestName;
		if(Constants.logEnable) Log.d(TAG,"responseCode :" + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK && result != null) {
			//if(Constants.logEnable)Log.i(TAG, "success! HttpURLConnection.HTTP_OK result != null");
			resultBean.success = true;
			resultBean.responseData = result;
		} else if(responseCode > 0){
			if(Constants.logEnable) Log.e(TAG, "NETWORK_SERVER_NO_RESPONSE: " + responseCode);
			resultBean.what = Constants.NETWORK_SERVER_NO_RESPONSE;
			resultBean.serverNoResponse = true;
		} else if(responseCode < 0){
			if(Constants.logEnable) Log.e(TAG, "NETWORK_CONNECTION_ERROR: " + responseCode);
			resultBean.what = Constants.NETWORK_CONNECTION_ERROR;
			resultBean.connectionError = true;
		}
		try {
			parser.parser(resultBean);
		} catch (Exception e) {
			if(Constants.logEnable) Log.e(TAG,"exception:"+e.getMessage());
		}
	}
	protected void onCancelled() {
		if(Constants.logEnable) Log.e(TAG,"AsyncTask onCancelled!");
	}

	private void checkMethodType(String methodType) {
		if(StringUtils.checkNULL(methodType)!=null)
		{
			this.methodType=methodType;
			if(methodType.equals("GET"))
			{
				doOutput=false;
			}
		}
	}
}
