package com.example.tlstest.webservice;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;


public class ServiceInstanceRequester {

	private boolean isNeedResponse=true;
	private ServiceParser parser = new ServiceParser();
	private ServiceCommunication serviceTask;
    static Context context;
	
	public static ServiceInstanceRequester getInstance(Context _context){
		context = _context;
		return new ServiceInstanceRequester();
	}
	
	public void request(Handler handler, String... params) {
		isNeedResponse=handler!=null;
		parser.setHandler(handler);
		serviceTask=new ServiceCommunication(context, parser);

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
			serviceTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		}else{
			serviceTask.execute(params);
		}
	}
	public void request(Handler handler, String methodType, String... params) {
		isNeedResponse=handler!=null;
		parser.setHandler(handler);
		serviceTask= new ServiceCommunication(context, parser,methodType);

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
			serviceTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		}else{
			serviceTask.execute(params);
		}
	}
	public void request(Handler handler, boolean assignUrl, String... params) {
		isNeedResponse=handler!=null;
		parser.setHandler(handler);
		serviceTask= new ServiceCommunication(context, parser,assignUrl);

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
			serviceTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		}else{
			serviceTask.execute(params);
		}
	}
	public void request(Handler handler, boolean assignUrl, String methodType, String... params) {
		isNeedResponse=handler!=null;
		parser.setHandler(handler);
		serviceTask= new ServiceCommunication(context, parser,assignUrl,methodType);

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
			serviceTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		}else{
			serviceTask.execute(params);
		}
	}

	public void cancel(){
		if(serviceTask!=null&&isNeedResponse){
			serviceTask.cancel(true);
			serviceTask=null;
		}
	}
}
