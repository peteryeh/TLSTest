package com.example.tlstest.webservice;

public class SeviceResponseBean {
	public int what;
	public int handleRequestName;
	public String methodName;
	public boolean success = false;
	public boolean serverNoResponse = false;
	public boolean connectionError = false;
	public int responseCode;
	public Object responseData;
}
