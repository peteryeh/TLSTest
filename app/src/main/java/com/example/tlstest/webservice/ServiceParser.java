package com.example.tlstest.webservice;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.tlstest.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;



public class ServiceParser {
	private static final String TAG = "ServiceParser";
	private Handler handler;
	private JSONObject jsonContent;
	

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public void parser(SeviceResponseBean response) {
		if(handler==null){
			if(Constants.logEnable) Log.e(TAG, "handler is null!");
			return;
		}
		Message msg = Message.obtain();
		msg.what = response.what;
		msg.arg1=response.handleRequestName;
//		if(Constants.logEnable)Log.i(TAG,"msg.what:"+msg.what);
		if (response.success && response.responseData != null) {
			String content =  (String) response.responseData;
			//if(Constants.logEnable)Log.v(TAG, "content :"+content);
			try {
				jsonContent = new JSONObject(content.trim());
				msg.obj = jsonContent;
			} catch (JSONException e) {
				if(Constants.logEnable) Log.e(TAG,"JSONException:"+e.getMessage());
			}
			//if(Constants.logEnable)Log.i(TAG,"content:"+content);
			

		} else if(response.serverNoResponse){
			if(Constants.logEnable) Log.v(TAG, "serverNoResponse");
		} else if(response.connectionError){
			if(Constants.logEnable) Log.v(TAG, "connectionError");
		} else {

			String content =  (String) response.responseData;
			if(Constants.logEnable) Log.e(TAG,"content:"+content);

		}
		handler.sendMessage(msg);
		
	}

	public void createTestDialog(int index){
		Message msg = Message.obtain();
		msg.what = index +2;
		msg.obj = String.valueOf(index);
		handler.sendMessage(msg);
	}

//	public void createTestDialogString(String msgStr){
//		Message msg = Message.obtain();
//		msg.what = Constants.HANDLE_TEST_STRING;
//		msg.obj = msgStr;
//		handler.sendMessage(msg);
//	}
	
}
