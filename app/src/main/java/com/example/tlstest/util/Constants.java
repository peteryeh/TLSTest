package com.example.tlstest.util;

public class Constants {
    public static final boolean logEnable=true;
    public static final String STRING_ENCODE = "UTF-8";
    public static final String DEFAULT_IP="";


    public static final int HANDLE_REQUEST_GET_MAINPAGE_CAMPAIGN = 1;
    public static final int NETWORK_SERVER_NO_RESPONSE = -5;
    public static final int NETWORK_CONNECTION_ERROR = -6;

    public static final String METHOD_GET_MAINPAGE_CAMPAIGN="";
    public static final String JSON_TOKEN="token";

    public static final String GET = "GET";
    public static final String POST = "POST";
}
