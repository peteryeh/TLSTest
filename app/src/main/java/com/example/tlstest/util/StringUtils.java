package com.example.tlstest.util;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class StringUtils {

	private static final String TAG= "StringUtils";
	
	public static String checkNULL(String str){
		if("".equals(str)|| str == null){
			return null;
		}else{
			return str;
		}
	}
	public static String checkNULL(String str, String strDefault){
		if("".equals(str)|| str == null){
			return strDefault;
		}else{
			return str;
		}
	}
	
	public static String getSpecificFormatString(String strFormat, String strData){
		
		
		if(checkNULL(strFormat)!=null && checkNULL(strData) != null){
			return String.format(strFormat, strData);
		}else{
			return null;
		}
	}
	
	public static<T> T getJSON(JSONObject receiveJSON, String key ) {
		try {
			return (T) (((receiveJSON != null && receiveJSON.has(key) && !receiveJSON.isNull(key))) ? receiveJSON.get(key) : null);
		}catch(JSONException e){
			if(Constants.logEnable) Log.e(TAG,"JSONObject :"+e.getMessage());
			return null;
		}
	}
}
